//
//  KTPhotoBrowserGlobal.m
//  Sample
//
//  Created by Kirby Turner on 2/11/10.
//  Copyright 2010 White Peak Software Inc. All rights reserved.
//

#import "KTPhotoBrowserGlobal.h"
#import <UIKit/UIKit.h>

///////////////////////

NSString * KTPathForBundleResource(NSString *relativePath) {
   NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
   return [resourcePath stringByAppendingPathComponent:relativePath];
}

///////////////////////

UIImage * KTLoadImageFromBundle(NSString *imageName) {
    NSString *relativePath = [NSString stringWithFormat:@"KTPhotoBrowser.bundle/images/%@", imageName];
    NSString *path  = KTPathForBundleResource(relativePath);
    
    if ([[UIScreen mainScreen] scale] == 2.0) {
        path = [path stringByReplacingOccurrencesOfString:@".png" withString:@"@2x.png"];
    }
    NSData *data = [NSData dataWithContentsOfFile:path];
    if ([[UIScreen mainScreen] scale] == 2.0) {
        return [UIImage imageWithData:data scale: 2.0];
    }
    return [UIImage imageWithData:data];
}